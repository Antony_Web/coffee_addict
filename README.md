# Coffee Addict

Projet synthèse production session 04

INSTRUCTION POUR PARTIR LE PROJET:
==================================

AVANT TOUT, npm install DANS LE DOSSIER RACINE DU PROJET (coffee_addict)

(démarrez le projet en 2 bash terminals différents)
POUR DÉMARRER REACT: root folder -> npm start
POUR DÉMARRER LE BACKEND: root folder -> cd api -> npm run devStart

ASSUREZ-VOUS D'AVOIR IMPORTÉ DANS VOTRE MySQL LE FICHIER DB DU PROJET (disponible dans le dossier racine du projet (coffee_addict)), ET DÉMARREZ VOTRE SERVEUR MySQL (soit avec WAMPP, MAMPP, XAMPP). SINON LE PROJET NE TROUVERA PAS LA DATABASE EN QUESTION.