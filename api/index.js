// REQUIRED DEPENDENCIES
const express = require("express")
const app = express()
const moment = require('moment');
const cors = require("cors")
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const session = require("express-session")
const mysql = require("mysql")
const bcrypt = require("bcrypt")
const { json } = require("express")
const saltRounds = 10

// Cors options

// DATABASE FETCHING
const db = mysql.createPool({
  host:       "localhost",
  user:       "root",
  password:   "",
  database:   "coffeeAddictDB",
  timezone: 'utc'
})

// MIDDLEWARES
app.use(cors({
  origin: ["http://localhost:3000"],
  methods: ["GET", "POST", "PUT", "DELETE"],
  headers: ["Origin", "X-Requested-With", "Content-Type"],
  credentials: true
}))
app.use(cookieParser())
app.use(express.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(session({
  key: "testkey",
  secret: "secretUserSession",
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 1000 * 60 * 60 * 2 }
}))
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Authorization");
  next();
});


//                                                        =============  API FUNCTIONS =============


//                                                       ============= FETCH INFORMATION =============

// LOGIN
app.get("/api/login", (req, res) => {
  if (req.session.user) {
    res.setHeader("Content-Type", "text/html")
    res.send({loggedIn: true, user: req.session.user})
    res.end()
  } else {
    res.send({loggedIn: false})
  }
})

// LOGOUT
app.get ("/api/logout", (req, res) => {
  if (req.session.user) {
    req.session.destroy()
    console.log("Logged out!")
    res.send({loggedIn: false})
  } else {
    console.log("Error: couldn't log out.")
  }
})

// FETCH USER DETAILS
app.get('/api/get', (req, res)=> {
    const sqlSelect = "SELECT * FROM users"
    db.query(sqlSelect, (err, result) => {
        res.send(result)
    })
})                                          //THEY'RE BOTH THE SAME*****

// Get all the users
app.get('/api/users', (req, res)=> {
  const sqlSelect = "SELECT * FROM users"
  db.query(sqlSelect, (err, result) => {
      res.send(result)
  })
})

// Get a user by id
app.get(`/api/users/:id`, (req, res)=> {
  db.query("SELECT * FROM users WHERE id = ?;",
  req.params.id, (err, result) => {
    if (!err) {
      res.send(result);
    } else {
      console.log('erreur', err);
    }
  })
})


// Get all the posts
app.get('/api/posts', (req, res)=> {
  db.query("SELECT * FROM posts",
  (err, result) => {
      if (err){
        console.log('erreur')
      }else {
        res.send({result})
      }
  })
})

// Get all the posts
app.get('/api/friends/posts/:user_id', (req, res)=> {
  const user_id = req.params.user_id
  db.query("SELECT * FROM posts INNER JOIN users ON posts.user_id = users.id ORDER BY created_at DESC", user_id,
  (err, result) => {
      if (err){
        console.log('erreur')
      }else {
        console.log('resultat de posts/:user_id', result)
        res.send({result})
      }

  })
})

// Get all the posts
app.get('/api/posts/:user_id', (req, res)=> {

  const user_id = req.params.user_id
  
  db.query("SELECT * FROM posts WHERE user_id = ? ORDER BY `created_at` DESC", user_id,
  (err, result) => {
      if (err){
        console.log('erreur')
      }else {
        res.send({result})
      }
      
  })
})

// Add Post 
app.post("/api/posts/add", (req, res) => {

  const title = req.body.title
  const user_id = req.body.user_id
  const content = req.body.content

  db.query(`INSERT INTO posts (title, user_id, content, created_at) VALUES (?,?,?,NOW());`, 
  [title, user_id, content],

  function (error, results) {
    if (error) {
      console.log(error,'POST NON ENREGISTRÉ !')
    } else {
      res.send({ posts: results });
      console.log('POST ENREGISTRÉ !', results);
    }
  });
})

// Add Meet up
app.post("/api/meets/add", (req, res) => {

  const invited_id = req.body.invited_id
  const host_id = req.body.host_id
  const date = req.body.date.toLocaleString()
  const place = req.body.place

  db.query(`INSERT INTO meetups (invited_id, host_id, date, place, created_at) VALUES (?,?,?, ?, NOW());`, 
  [invited_id, host_id, date, place],

  function (error, results) {
    if (error) {
      console.log(error,'meet up NON ENREGISTRÉ !')
    } else {
      res.send({ meets: results });
      console.log('meet up ENREGISTRÉ !', results);
    }
  });
})

//ELECT * FROM posts INNER JOIN users ON posts.user_id = users.id ORDER BY

// Get all the posts
app.get('/api/meets/:user_id', (req, res)=> {
  db.query("SELECT * FROM meetups WHERE (host_id = ? OR invited_id = ?);", [req.params.user_id, req.params.user_id],
  (err, result) => {
      if (err){
        console.log('erreur')
      }else {
        res.send({result})
      }
      
  })
})


// Get le user id for update
app.get('/api/edit/:id', (req, res) => {
  db.query("SELECT * FROM users WHERE id = ?;",
  req.params.id, (err, result) => {
    if (!err) {
      res.send(result);
      //console.log('le user get:', result);
    } else {
      //console.log('erreur', err);
    }
  })
});

// Update le userId
app.put('/api/edit/:id', (req, res) => {

  const id = req.params.id
  const username = req.body.username
  const firstName = req.body.firstName
  const lastName = req.body.lastName
  const password = req.body.password
  const email = req.body.email
  const gender = req.body.gender

  bcrypt.hash(password,saltRounds, (err, hash) => {
    if (err) {
      console.log(err)
    }

    db.query('UPDATE users SET username = ?, firstName = ?, lastName = ?, email = ?, password = ?, gender = ? WHERE id = ?', 
    [username, firstName, lastName, email, hash, gender, id], 
    function (error, results) {
      if (error) {
        console.log(error,'USER NON ENREGISTRÉ !')
      } else {
        res.send({loggedIn: true, loggedUser: results});
        console.log('USER ENREGISTRÉ !', results);
      }
    });

  })
});

// Get a user by id
app.get(`/api/requests/:id`, (req, res)=> {
  db.query("SELECT * FROM requests WHERE id = ?;",
  req.params.id, (err, result) => {
    if (!err) {
      res.send(result);
    } else {
      console.log('erreur', err);
    }
  })
})
// GET FRIEND LIST
app.get("/api/friends/:userID", (req, res) => {
  db.query("SELECT * FROM requests WHERE is_confirmed = 1 AND (receiver_id = ? OR sender_id = ?);", 
  [req.params.userID, req.params.userID], (err, result) => {
    res.send(result);
  })
})


// FRIEND REQUESTS PENDING
app.get("/api/pending/:receiverID", (req, res)=> {
  console.log(req.params)
  db.query("SELECT * FROM requests WHERE receiver_id = ? AND is_confirmed = 0;", 
  req.params.receiverID, (err, result) => {
    if(err){
      console.log(err)
    } else {
      console.log('les pending:',result);
      res.send({setFriends: false, result});
    }

  }); 
})

// GET ALL REQUESTS
app.get("/api/requests", (req, res)=> {
  const sqlSelect = "SELECT * FROM requests"
  db.query(sqlSelect, (err, result) => {
    if (err){
      res.send(err)
    } else {
      res.send(result)
    }
  })
})


// Get a request by id
app.get(`/api/requests/:id`, (req, res)=> {
  db.query("SELECT * FROM requests WHERE id = ?;",
  req.params.id, (err, result) => {
    if (!err) {
      res.send(result);
    } else {
      console.log('erreur', err);
    }
  })
})

// REGISTER
app.post("/api/register", (req, res)=> {

  const username  = req.body.username
  const password  = req.body.password
  const email     = req.body.email
  const firstName = req.body.firstName
  const lastName  = req.body.lastName
  const gender    = req.body.gender
  const sqlInsert = "INSERT INTO users (username, password, email, firstName, lastName, gender) VALUES (?,?,?,?,?,?);"

  bcrypt.hash(password,saltRounds, (err, hash) => {
    if (err) {
      console.log(err)
    }
    db.query(sqlInsert, [username, hash, email, firstName, lastName, gender], (err, result)=> {
      if(err){
        console.log(err)
      } else {
        console.log(result)
      }
      console.log("successfully inserted!!")
      
    })
  })
})

// GET FRIEND LIST
app.get("/api/friends/:userid", (req, res) => {
  db.query("SELECT * FROM requests WHERE is_confirmed = 1 AND (receiver_id = ? OR sender_id = ?);", 
  [req.params.userid, req.params.userid], (err, result) => {
    res.send({setFriends: true, result});
  })
})



// Get all the posts
app.get('/api/friends/posts/:user_id', (req, res)=> {
  const user_id = req.params.user_id
  db.query("SELECT * FROM posts INNER JOIN users ON posts.user_id = users.id ORDER BY `created_at` DESC", user_id,
  (err, result) => {
      if (err){
        console.log('erreur')
      }else {
        console.log('resultat de posts/:user_id', result)
        res.send({result})
      }
      
  })
})

// SEND FRIEND REQUEST
app.post("/api/add-friend", (req, res)=> {

  const receiver_id = req.body.receiver
  const sender_id   = req.body.sender

  db.query("INSERT INTO requests (sender_id, receiver_id, is_confirmed) VALUES (?,?,0)", [sender_id, receiver_id], 
  (err, result) => {
    if (err) {
      console.log(err)
    } else {
      console.log(result, "Friend request sent.")
    }
  })
})
// Get session for logged in user
app.post("/api/login", (req, res) => {
  const username = req.body.username
  const password = req.body.password

  db.query(
      "SELECT * FROM users WHERE username = ?;",
      username,
      (err, result) => {
          if (err) {
              res.send({err: err})
          }

          if (result.length > 0) {
              bcrypt.compare(password, result[0].password, (error, response) => {
                if (response) {
                  req.session.user = result;
                  console.log(req.session.user)
                  res.send(result)
                } else {
                  res.send({ message: "Wrong username/password. Please try again."})
                }
              })
          } else {
              res.send({ message: "This user does not exist." })
          }
      }
  )
})

//                                                       ============= UPDATES =============

// CONFIRM FRIEND REQUEST
app.put("/api/confirmRequest/:id", (req, res) => {
  const receiver_id = req.body.receiver
  const id = req.params.id
  db.query("UPDATE requests SET is_confirmed = 1 WHERE (receiver_id = ? AND id = ?);", [receiver_id, id], (err, result) => {
    if (err) {
      console.log(err, "An error happened on request confirmation.")
    } else {
      console.log(result, "Friend request confirmed!")
    }
  }
)})

// REPUTATION +2 ON FRIEND REQUEST

app.put("/api/plus2reputation/:userid", (req, res) => {
  const user_id = req.params.userid

  db.query("UPDATE users SET points = points + 2 WHERE id = ?", user_id, (err, result) => {
    if (err) {
      console.log(err, "Can't increment points by 2, see error.")
    } else {
      console.log(result, "Incremented points by 2!")
    }
  })
})

// REPUTATION +10 ON MAKING A POST

app.put("/api/plus10reputation/:userid", (req, res) => {
  const user_id = req.params.userid

  db.query("UPDATE users SET points = points + 10 WHERE id = ?", user_id, (err, result) => {
    if (err) {
      console.log(err, "Can't increment points by 10, see error.")
    } else {
      console.log(result, "Incremented points by 10!")
    }
  })
})

// REPUTATION +25 FOR CREATING A MEETUP

app.put("/api/plus25reputation/:userid", (req, res) => {
  const user_id = req.params.userid

  db.query("UPDATE users SET points = points + 25 WHERE id = ?", user_id, (err, result) => {
    if (err) {
      console.log(err, "Can't increment points by 25, see error.")
    } else {
      console.log(result, "Incremented points by 25!")
    }
  })
})

//                                                       ============= DELETION =============

// REFUSE FRIEND REQUEST
app.delete("/api/refuse/:requestid", (req, res) => {
  const id = req.params.requestid
  console.log(id)
  db.query("DELETE FROM requests WHERE (id = ? AND is_confirmed = 0);", [id], (err, result) => {
    if (err) {
      console.log(err, "Cannot delete this request.")
    } else {
      console.log(result, "You've refused a friend request.")
    }
  })
})

// DELETE FRIEND FROM YOUR FRIENDS LIST
app.delete("/api/deleteFriend/:requestid", (req, res) => {
  const id = req.params.requestid
  db.query("DELETE FROM requests WHERE (id = ? AND is_confirmed = 1);", [id], (err, result) => {
    if (err) {
      console.log(err, "Cannot delete this friend.")
    } else {
      console.log(result, "You've deleted a friend.")
    }
  })
})


// LISTENING PORT
app.listen(3001, () => {
    console.log("Server up and running on 3001 !")
})