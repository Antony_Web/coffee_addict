-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : lun. 13 déc. 2021 à 18:57
-- Version du serveur :  5.7.34
-- Version de PHP : 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `coffeeAddictDB`
--

-- --------------------------------------------------------

--
-- Structure de la table `meetups`
--

CREATE TABLE `meetups` (
  `id` bigint(20) NOT NULL,
  `invited_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `date` datetime NOT NULL,
  `place` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `host_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `meetups`
--

INSERT INTO `meetups` (`id`, `invited_id`, `created_at`, `date`, `place`, `host_id`) VALUES
(8, 1, '2021-12-08 09:57:44', '2021-12-16 20:00:00', 'Café Blake', 5),
(11, 5, '2021-12-08 14:55:04', '2021-12-10 18:00:00', 'Café Tim Horton', 1);

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 'Title 1', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:03:59', NULL),
(2, 2, 'Title 1', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:05:38', NULL),
(3, 2, 'Title 2', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:05:47', NULL),
(4, 2, 'Title 3', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:05:57', NULL),
(5, 3, 'Title 1', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:07:23', NULL),
(6, 3, 'Title 2', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:09:42', NULL),
(7, 3, 'Title 3', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:09:49', NULL),
(8, 2, 'Title 4', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:10:44', NULL),
(9, 3, 'Title 4', 'Lorem ipsum dolor sit amet.', '2021-11-22 23:11:02', NULL),
(10, 1, 'Titre 2', 'Lorem ipsum dolor sit amet.', '2021-11-24 13:43:16', NULL),
(11, 1, 'Title 3', 'Lorem ispum dolor sit amet.', '2021-12-08 14:34:37', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `requests`
--

CREATE TABLE `requests` (
  `id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `receiver_id` bigint(20) NOT NULL,
  `is_confirmed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `requests`
--

INSERT INTO `requests` (`id`, `sender_id`, `receiver_id`, `is_confirmed`) VALUES
(1, 2, 1, 1),
(2, 3, 1, 1),
(3, 4, 3, 1),
(5, 5, 1, 1),
(7, 2, 3, 0),
(8, 6, 3, 0),
(10, 2, 4, 0),
(11, 2, 5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `points` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `firstName`, `lastName`, `gender`, `points`) VALUES
(1, 'Samuel', '$2b$10$tONDQB2uN9r7XgZ2txIUsuDcVlcuqvrXFz8QwEHejB9Wh.6uywVO.', 'samuel@hotmail.fr', 'Samuel', 'Brideau', 'male', 0),
(2, 'Laurie', '$2b$10$m/4L9jwOxMENB.5fqRe1cevQ47DGwHLmezfFFqgJ9/wEwwK5TGcla', 'lboilard@outlook.com', 'Laurie', 'Boilard', 'female', 0),
(3, 'Isabelle', '$2b$10$YOXNN1sh1ts9Qdd/HSJiRe08sfFnIu2jfhm6FykbatXMzVeUqjyWS', 'Isabelle@hotmail.com', 'Isabelle', 'Gentille', 'female', 0),
(4, 'allo', '$2b$10$52Ju2Tt3Gg61ttZqV4c.T.PIWTBChbZeD/vHMHBezIKMcJIo3tDg2', 'allo@123.com', 'allo', 'allo', 'female', 0),
(5, 'Martin', '$2b$10$kdvLf2.tgDXKWfp5bFVDyObLzeQA.PRTz1lLhfXbaCux6RVvOBaAy', 'martin@outlook.com', 'Martin', 'Boilard', 'male', 0),
(6, 'Kimboil', '$2b$10$2bdAQrlSqeCmJrGD6ZIhYuecqGnHb2CbbrcnhfvaFMnTdyfqow4bm', 'lboilard@outlook.com', 'Kim', 'Boilard', 'female', 0),
(7, 'Partick', '$2b$10$uj6KDUOCPunoxYz6OXz1F.udQHaY78wJHIhwFsFff/HEaofB.Knf6', 'Partick@hotmail.com', 'Partick', 'Partick', 'male', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `_prisma_migrations`
--

CREATE TABLE `_prisma_migrations` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checksum` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finished_at` datetime(3) DEFAULT NULL,
  `migration_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logs` text COLLATE utf8mb4_unicode_ci,
  `rolled_back_at` datetime(3) DEFAULT NULL,
  `started_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `applied_steps_count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `_prisma_migrations`
--

INSERT INTO `_prisma_migrations` (`id`, `checksum`, `finished_at`, `migration_name`, `logs`, `rolled_back_at`, `started_at`, `applied_steps_count`) VALUES
('ada69277-1ff7-4d41-847a-13795c15e2dd', '3c975ce72f6cd888db0cd12b35eeab9169faffab856c86f6a266525adf29d292', '2021-11-23 04:02:33.002', '20211123040232_v1', NULL, NULL, '2021-11-23 04:02:32.729', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `meetups`
--
ALTER TABLE `meetups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_host_id_foreign` (`host_id`),
  ADD KEY `users_invited_id_foreign` (`invited_id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Index pour la table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_receiver_id_foreign` (`receiver_id`),
  ADD KEY `users_sender_id_foreign` (`sender_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `_prisma_migrations`
--
ALTER TABLE `_prisma_migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `meetups`
--
ALTER TABLE `meetups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `meetups`
--
ALTER TABLE `meetups`
  ADD CONSTRAINT `users_host_id_foreign` FOREIGN KEY (`host_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_invited_id_foreign` FOREIGN KEY (`invited_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `users_receiver_id_foreign` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
