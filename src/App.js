import React, {useState, useEffect} from 'react';
import {Redirect, BrowserRouter, Switch, Route} from 'react-router-dom';
import './App.css';
import Home from './views/Home';
import Recherche from './views/Recherche';
import Navigation from './components/Navigation';
import Footer from './components/Footer';
import Connexion from './views/Connexion';
import Register from './views/Register';
import Profil from './views/Profil';
import Axios from 'axios';
import { LoginContext } from './contexts/loginStatus';
import AddUser from "./components/AddUser";
import EditUser from "./components/EditUser";
import User from './views/User';
import FriendRequest from './views/FriendRequest';
import AddPost from './components/posts/addPost';
import AddMeets from './components/meets/addMeets';
import MeetsList from './components/meets/Meets';
import PostList from './components/posts/Post';



function App() {

  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(()=> { //CONNECTION CHECK
    Axios.get("http://localhost:3001/api/login").then((res) => {
      if (res.data.loggedIn === true) {
        console.log(res.data.user[0])
        setLoggedIn(true)
      } else {
        setLoggedIn(false)
      }
    })
  }, [])

    return (
      <LoginContext.Provider value={{ loggedIn, setLoggedIn }}>
      <div className="App">
      <BrowserRouter>
      <Navigation/>
      <Switch>

      <Route path='/posts/add' component={AddPost} />
      <Route path='/posts' component={PostList} />
      
      <Route path='/users/:id' component={User} />

      <Route exact path="/friendrequest" > 
        {loggedIn ? <FriendRequest />  : <Connexion/>}
      </Route>

      <Route path="/add"> <AddUser /> </Route>
      <Route path="/edit/:id" component={EditUser}/>
      <Route path="/" exact component={Home}/>
     
      <Route path="/Recherche" exact component={Recherche}/>
      <Route path="/Connexion" exact>
        {loggedIn ? <Redirect to="/" /> : <Connexion/>}
      </Route>

      <Route path="/addMeets" exact component={AddMeets}/>
      <Route path="/meets" exact component={MeetsList}/>

      <Route path="/Register" exact component={Register}/>
      <Route path="/Profil" exact>
        {loggedIn ? <Profil /> : <Connexion/>}
      </Route>
      </Switch>
      </BrowserRouter>
      <Footer/>
      </div>
      </LoginContext.Provider>
    )
}

export default App;
