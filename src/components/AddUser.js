import React, { Component }  from 'react';
import { useState } from 'react'
import axios from "axios";
import { useHistory } from 'react-router-dom';
 
const AddUser = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [gender, setGender] = useState("");

    const history = useHistory();
 
    const saveUser = async (e) => {
        e.preventDefault();
        await axios.post('http://localhost:3001/api/users',{
            username: username,
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            gender: gender,
        });
        history.push("/");
    }
 
    return (
        <div>
            <form onSubmit={ saveUser }>
                <div className="field">
                    <label className="label">username</label>
                    <input 
                        className="input"
                        type="text"
                        placeholder="username"
                        value={ username }
                        onChange={ (e) => setUsername(e.target.value) }
                    />
                </div>
 
                <div className="field">
                    <label className="label">firstname</label>
                    <input 
                        className="input"
                        type="text"
                        placeholder="firstname"
                        value={ firstName }
                        onChange={ (e) => setFirstName(e.target.value) }
                    />
                </div>

                <div className="field">
                    <label className="label">lastname</label>
                    <input 
                        className="input"
                        type="text"
                        placeholder="lastname"
                        value={ lastName }
                        onChange={ (e) => setLastName(e.target.value) }
                    />
                </div>

                <div className="field">
                    <label className="label">email</label>
                    <input 
                        className="input"
                        type="text"
                        placeholder="email"
                        value={ email }
                        onChange={ (e) => setEmail(e.target.value) }
                    />
                </div>

                <div className="field">
                    <label className="label">password</label>
                    <input 
                        className="input"
                        type="text"
                        placeholder="password"
                        value={ password }
                        onChange={ (e) => setPassword(e.target.value) }
                    />
                </div>

                <div className="radio-group field">
                    <label className="btn " for="gender">Homme / Il</label>
                    <input type="radio" name="gender" id="gender1" autocomplete="off" value="male" onChange={(e) => {
                            setGender(e.target.value)
                        }}/>
                    <label className="btn " for="gender">Femme / Elle</label>
                    <input type="radio" name="gender" id="gender2" autocomplete="off" value="female" onChange={(e) => {
                            setGender(e.target.value)
                        }}/>
                    <label className="btn " for="gender">Autres / Iel</label>
                    <input type="radio" name="gender" id="gender3" autocomplete="off" value="other" onChange={(e) => {
                            setGender(e.target.value)
                        }}/>
                </div>
 
                <div className="field">
                    <button className="button is-primary">Save</button>
                </div>
            </form>
        </div>
    )
}
 
export default AddUser