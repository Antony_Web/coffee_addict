import React from "react";
import Card from 'react-bootstrap/Card'
import CardGroup from 'react-bootstrap/CardGroup'
import Carousel from 'react-bootstrap/Carousel'
// import Button from 'react-bootstrap/Button'


const ArticleBlog = () => {
return(
<div>


<Carousel>
  <Carousel.Item>
  <CardGroup className="articleBlog">
  <Card>
    <Card.Body>
      <Card.Title>Steve Doe</Card.Title>
      <Card.Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <small className="text-muted">Publié le 8 Octobre 2021</small>
    </Card.Footer>
  </Card>
  <Card> 
    <Card.Body>
      <Card.Title>Beatrice Doe</Card.Title>
      <Card.Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <small className="text-muted">Publié le 3 Octobre 2021</small>
    </Card.Footer>
  </Card>
  <Card>
    <Card.Body>
      <Card.Title>Pauline Doe</Card.Title>
      <Card.Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <small className="text-muted">Publié le 17 Septembre 2021</small>
    </Card.Footer>
  </Card>
  </CardGroup>
  </Carousel.Item>
  
  <Carousel.Item>
  <CardGroup className="articleBlog">
  <Card>
    <Card.Body>
      <Card.Title>Amélie Doe</Card.Title>
      <Card.Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <small className="text-muted">Publié le 24 Aout 2021</small>
    </Card.Footer>
  </Card>
  <Card>
    <Card.Body>
      <Card.Title>Karine Doe</Card.Title>
      <Card.Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <small className="text-muted">Publié le 27 Juillet 2021</small>
    </Card.Footer>
  </Card>
  <Card href="./views/Blog">
    <Card.Body>
      <Card.Title>Samuel Doe</Card.Title>
      <Card.Text>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  
      </Card.Text>
    </Card.Body>
    <Card.Footer>
      <small className="text-muted">Publié le 6 Juillet 2021</small>
    </Card.Footer>
  </Card>
  </CardGroup>
  </Carousel.Item>
</Carousel>
{/* <Button variant="outline-info" className="bouton" href="/Blog">{'Voir nos articles'}</Button> */}
</div>
 
);
};

export default ArticleBlog;