import React from "react";
import FirstImage from '../images/FirstImage.jpg'
import SecondImage from '../images/SecondImage.jpg'
import ThirdImage from '../images/ThirdImage.jpg'
import Carousel from 'react-bootstrap/Carousel'

const Caroussel = () => {
  return (

<Carousel>

  <Carousel.Item>
    <img
      className="d-block w-100"
      src={FirstImage}
      alt="First slide"
    />
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={SecondImage}
      alt="Second slide"
    />
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src={ThirdImage}
      alt="Third slide"
    />
  </Carousel.Item> 
</Carousel>
  );
};

export default Caroussel;
