/* eslint-disable react-hooks/exhaustive-deps */
import React, { Component }  from 'react';
import { useState, useEffect } from 'react'
import axios from "axios";
import { useHistory, useParams } from 'react-router-dom';
 
const EditUser = () => {
    
    const history = useHistory();
    const { id } = useParams();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [gender, setGender] = useState("");
    const [user, setUser] = useState("");
    const [loggedUser, setLoggedUser] = useState([]);

    const updateUser = () => {
        axios.put(`http://localhost:3001/api/edit/${id}`,{
            username: username,
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            gender: gender,
        }).then((res)=> {
          if (res){
            console.log("the user has not updated")
          } else {
            setLoggedUser([res.data.user[0]])
            console.log("The user is updated !:", res.data);
            return res.data
              
          }
        })
      };
 
 
    useEffect(() => {
        getUserById();
    }, []);
 
    const getUserById = async () => {
        const res = await axios.get(`http://localhost:3001/api/users/${id}`);
        setUser(res.data[0])
        setUsername(res.data[0].username);
        setFirstName(res.data[0].firstName);
        setLastName(res.data[0].lastName);
        setEmail(res.data[0].email);
        setPassword(res.data[0].password);
        setGender(res.data[0].gender);
        console.log('user à updater', res.data[0]);
    }

    
 
    return (
        <div className="backg">
            <form onSubmit={ updateUser } className="form-register" action="/Profil">
                <div className="form-group">
                    <label className="inscription-label">Prénom</label>
                    <input
                        className="form-control"
                        type="text"
                        placeholder="firstname"
                        value={ firstName }
                        onChange={ (e) => setFirstName(e.target.value)
                         }
                    />
                </div>

                <div className="field form-group">
                    <label className="label">Nom</label>
                    <input 
                        className="form-control"
                        type="text"
                        placeholder="lastname"
                        value={ lastName }
                        onChange={ (e) => setLastName(e.target.value) }
                    />
                </div>

                <div className="radio-group field">
                <label className="btn " for="gender">Votre sexe</label>
                    <input disabled value={gender}/>
                    <label className="btn " for="gender">Homme / Il</label>
                    <input type="radio" name="gender" id="gender1" autocomplete="off" value="male" onChange={(e) => {
                            setGender(e.target.value)
                        }}/>
                    <label className="btn " for="gender">Femme / Elle</label>
                    <input type="radio" name="gender" id="gender2"  autocomplete="off" value="female" onChange={(e) => {
                            setGender(e.target.value)
                        }}/>
                    <label className="btn " for="gender">Autres / Iel</label>
                    <input type="radio" name="gender" id="gender3" autocomplete="off" value="other" onChange={(e) => {
                            setGender(e.target.value)
                        }}/>
                </div>

                <div className="form-group input">
                    <label className="label">Pseudo</label>
                    <input 
                        className="form-control"
                        type="text"
                        placeholder="username"
                        value={ username }
                        onChange={ (e) => setUsername(e.target.value) }
                    />
                </div>

                <div className="form-group input">
                    <label className="label">Courriel</label>
                    <input 
                        className="form-control"
                        placeholder="email"
                        value={ email }
                        onChange={ (e) => setEmail(e.target.value) }
                    />
                </div>

                <div className="form-group input">
                    <label className="label">Mot de passe</label>
                    <input 
                        className="form-control"
                        placeholder="password"
                        value={ password }
                        onChange={ (e) => setPassword(e.target.value) }
                    />
                </div>
 
                <div className="buttons_grid">
                    <button className="button">Modifier</button>
                </div>
            </form>
        </div>
    )
}
 
export default EditUser;