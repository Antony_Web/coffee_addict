import React, {useState, useEffect} from 'react'
import axios from 'axios';
import { LoginContext } from '../contexts/loginStatus';
import { useHistory } from 'react-router';
import { NavLink } from "react-router-dom";
import Recherche from "../views/Recherche";

const Navigation = () => {
    axios.defaults.withCredentials = true;

    const [loggedIn, setLoggedIn] = useState(false);
    const [loggedUser, setLoggedUser] = useState([]);
    const [id, setUserId] = useState('');
    const [id_, setUserId_] = useState('');
    const [pending, setPending] = useState([]);

    const history = useHistory();


    const logout = () => {
        axios.get("http://localhost:3001/api/logout").then((res) => {
          if (res.data.loggedIn === false) {
            history.push('/')
            setLoggedIn(false)
            window.location.reload(false);
            console.log("Logged out.")
          } else {
            console.log("You gotta login first.")
          }
        })
      }

      useEffect(()=> {
        axios.get("http://localhost:3001/api/login").then((res) => {
          if (res.data.loggedIn === true) {
            setUserId(res.data.user[0].id)
            setUserId_(res.data.user[0].id)
            getPending(res.data.user[0].id)
            setLoggedIn(true)
          } else {
            setLoggedIn(false)
          }
          
        })
      }, [])

      useEffect(()=> { //CONNECTION CHECK
        axios.get(`http://localhost:3001/api/users/${id}`)
        .then((res) => {
            setLoggedUser(res.data)
        })
      }, [id])

      const getPending = async (userid) => {
        const requests = await axios.get(`http://localhost:3001/api/pending/${userid}`);
        setPending(requests.data.result)
        console.log(requests.data, 'resultat du pending')
    }



    return (
        <LoginContext.Provider value={{ loggedIn, setLoggedIn }}>
        
        
        <div className="navbar navbar-expand-lg navbar-light">
          <div className="container-fluid">

            <NavLink exact to="/" className="nav-link">
                accueil
            </NavLink>
            
            {loggedIn ?
            <>
            <Recherche className="nav-link" />
            
            <NavLink exact to="/Profil" className="nav-link">
              mon profil 
            </NavLink>

            <NavLink exact to="/FriendRequest" className="nav-link friends-aking">
                notifications <span className="badge">{pending.length}</span>
            </NavLink>

            <div className="mx-auto"></div>

            <NavLink to="#" className="nav-link user_link">
                {loggedUser.map((val, key) => {
                    return (
                        <p>Bonjour, {val.firstName} !</p>
                    )
                })}
              <button onClick={logout} className="button logout">Déconnexion</button>
            </NavLink> 
        </>
        
        : 
        <>

        <NavLink exact to="/Connexion" className="nav-link menu">
          <button className="button logout">Connexion</button>
        </NavLink>

        </>

        }
        </div>
        </div>
        
        </LoginContext.Provider>

    )
}

export default Navigation
