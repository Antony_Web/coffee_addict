import React, { useState, useEffect } from "react"
import Axios from "axios";
import Avatar from "../images/background/avatar1.png";

const Request = () => {

    Axios.defaults.withCredentials = true;
    const [loggedIn, setLoggedIn] = useState(false);
    const [loggedUser, setLoggedUser] = useState([]);
 
    // Connected user
    useEffect(()=> {
        const fetchLoggedUser = async () =>{
            const response = await Axios.get("http://localhost:3001/api/login")
              if (response.data.loggedIn === true) {
                setLoggedUser(response.data.user[0])
                setLoggedIn(true)
              } else {
                setLoggedIn(false)
              }
              getPending(response.data.user[0].id)
              getFriends(response.data.user[0].id)
        }
        fetchLoggedUser();
    }, [])

    // Fetch and show a single user's profile page by getting their ID.
    const getUserById = async (id) => {
        const response = await Axios.get(`http://localhost:3001/api/users/${id}`)
        return response.data[0];
    }

    const getRequestById = async (id) => {
        const response = await Axios.get(`http://localhost:3001/api/requests/${id}`)
        return response.data[0];
    }
    
    // Confirm a friend Request
    const confirmRequest = (receiverId, id) => {
        Axios.put(`http://localhost:3001/api/confirmRequest/${id}`, {
            receiver: receiverId,
            id: id
        }).then((res) => {
            if (res) {
                console.log('receiver data:::', res)
                console.log("Friend request confirmed.")
            } else {
            
            }
        })
    }

    // State for pending requests
    const [pending, setPending] = useState([])

    // Show pending requests
    const getPending = async (userid) => {
        console.log("Logged user", loggedUser);
        const requests = await Axios.get(`http://localhost:3001/api/pending/${userid}`);

        console.log(requests);

        const pendingResult = requests.data.result.map(async (request) => {
            const receiver = await getUserById(request.receiver_id);
            const sender = await getUserById(request.sender_id);
            const id_request = await getRequestById(request.id);

            console.log('le resultat wesh:', id_request, receiver, sender)
            return { id_request, receiver, sender };
        });

        console.log(await Promise.all(pendingResult));
        setPending(await Promise.all(pendingResult))
        return await Promise.all(pendingResult);
    }

    //DELETE FRIEND REQUEST
    const deleteRequest = (id) => {
        Axios.delete(`http://localhost:3001/api/refuse/${id}`).then((res) => {
        })
    }

    //GET FRIENDLIST
    const [friendlist, setFriendlist] = useState([])

    const getFriends = async (userid) => {
        const friendlist = await Axios.get(`http://localhost:3001/api/friends/${userid}`);

        console.log(friendlist)

        const pendingResult = friendlist.data.map(async (request) => {
            // receiver_id
            // sender_id
            if(request.receiver_id === userid) {
                return await getUserById(request.sender_id);
            } else {
                return await getUserById(request.receiver_id);
            }
        });

        console.log(await Promise.all(pendingResult));
        setFriendlist(await Promise.all(pendingResult))
        return await Promise.all(pendingResult);
    }

    function refreshPage() {
        window.location.reload(false);
    }


    
    return (
        <>
            
        <div className="wrapper">
            <div>
                <div className="pending-grid">
                {pending.map((pendingRequest, index) => (
                    <>
                    <div index={ index }>
                    <div className="asking">
                        <img className="img_friend" src={Avatar} alt="avataruser"/>
                        <p>{pendingRequest.sender.firstName} {pendingRequest.sender.lastName}</p>
                        <p>{pendingRequest.sender.email}</p>
                        <div className="pending buttons">
                            <button className="button confirm" onClick={() => { confirmRequest(pendingRequest.receiver.id, pendingRequest.id_request.id); refreshPage(); }}>Confirmer</button>
                            <button className="button refuse" onClick={() => { deleteRequest(pendingRequest.id_request.id); refreshPage(); }}>Refuser</button>
                        </div>
                    </div>
                    </div>
                    </>
                ))}
                </div>

            </div>

        </div>
        
        </>
    )


}

export default Request;