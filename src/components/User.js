import React, { useState, useEffect } from "react"
import { Link, useParams } from "react-router-dom";
import axios from "axios";    
import Avatar from "../images/background/avatar1.png";
import pictureUser1 from "../images/user1.jpg";

const User = () => {

    // Get ID from URL
    const params = useParams();
    const [users, setUsers] = useState([])
    const [friend, setFriend] = useState(false);
    const [user, setUser] = useState([])
    const [loggedUser, setLoggedUser] = useState(undefined);
    const [userConnected, setConnectedUser] = useState([]);

    const [friends, setFriends] = useState(false)

    useEffect(()=> {
        axios.get("http://localhost:3001/api/login")
        .then((res) => {
          if (res) {
            setLoggedUser([res.data.user[0]])
            setConnectedUser(res.data.user[0].id)
          } else {
          }
        })
      }, [])

    useEffect(() => {
        getUsers();
    }, []);

    const getUsers = () => {
        axios.get(`http://localhost:3001/api/users`)
            .then((res)=> {
                setUsers(res.data);
        })
    };

    useEffect(()=> { //CONNECTION CHECK
        axios.get(`http://localhost:3001/api/users/${params.id}`)
        .then((res) => {
            setUser(res.data)
            getPending(res.data[0].id)
            getFriends(res.data[0].id)
            getFriendsOfme(userConnected)
            //buttonAdd(userConnected)
        })
      }, [params.id])

      // Fetch and show a single user's profile page by getting their ID.
    const getUserById = async (id) => {
        const response = await axios.get(`http://localhost:3001/api/users/${id}`)
        return response.data[0];
    }

    // Send a friend request to a user
    const sendRequest = (userId) => {
        axios.post('http://localhost:3001/api/add-friend', {
            receiver: userId,
            sender: loggedUser[0].id
        }).then((res) => {
            if (res) {
                console.log("Friend request sent.")
            }
        })
    }

    // Reputation +2 for sender
    const repIncrement = (id) => {
        axios.put(`http://localhost:3001/api/plus2reputation/${id}`, (req, res) => {
        })
        console.log(id)
    }


    // State for pending requests
    const [pending, setPending] = useState([])

    // Show pending requests
    const getPending = async (userid) => {
        const requests = await axios.get(`http://localhost:3001/api/pending/${userid}`);
        const pendingResult = requests.data.result.map(async (request) => {
        const receiver = await getUserById(request.receiver_id);
        const sender = await getUserById(request.sender_id);

            return { receiver, sender };
        });
        setPending(await Promise.all(pendingResult))
        return await Promise.all(pendingResult);
    }

    const [add, setAdd] = useState("Ajouter +")
    const addFunction = (e) => {
        setAdd("Envoyé!")
    }

    //GET FRIENDLIST
    const [friendlist, setFriendlist] = useState([])

    const getFriends = async (userid) => {
        const friendlist = await axios.get(`http://localhost:3001/api/friends/${userid}`);

        //console.log(friendlist)

        const pendingResult = friendlist.data.map(async (request) => {
            // receiver_id
            // sender_id
            if(request.receiver_id === userid) {
                return await getUserById(request.sender_id);
            } else {
                return await getUserById(request.receiver_id);
            }
        });
        setFriendlist(await Promise.all(pendingResult))
        return await Promise.all(pendingResult);
    }

    const [friendlistOfme, setFriendlistOfme] = useState([])

    const getFriendsOfme = async (userid) => {
        const friendlistOfme = await axios.get(`http://localhost:3001/api/friends/${userid}`);
        const pendingResult = friendlistOfme.data.map(async (request) => {
            // receiver_id
            // sender_id
            if(request.receiver_id === userid) {
                return await getUserById(request.sender_id);
            } else {
                return await getUserById(request.receiver_id);
            }
        });
        setFriendlistOfme(await Promise.all(pendingResult))
        return await Promise.all(pendingResult);
    }

    const isFriend = []
    friendlist.forEach(function(user) {
      if (user.id === userConnected) {
        isFriend.push(true)
      } else {
        
      }
    });

    return (
        <>
        <div className="bg-blues background-login">
            <div className="wrapper">
                <div className="profil_grid">
                    <img className="img_profil" src={Avatar} alt="project number two."/>
                    {user.map((user, index) => {
                        return(
                        <>
                        <div className="profil_info">
                            <p>{user.username}</p>
                            <p>{user.firstName} {user.lastName}</p>
                            <p>{user.email}</p>
                            <p>{user.gender}</p>
                            <p>Status: 
                              { user.points < 6 ? " Débutant" : null}
                              { user.points > 5 && user.points < 26 ? " Apprentis" : null }
                              { user.points > 25 && user.points < 51 ? " Connaisseur" : null }
                              { user.points > 50 ? " Maître torréfacteur" : null}
                            </p>
                            <p>{user.points} points</p>
                        </div>

                        { isFriend[0] === true ?
                            <div className="buttons">
                                
                            </div>

                         : 
                         <div className="buttons">
                             {loggedUser.map((logged, index) => {
                                 return (
                                     <button className="button add" id="button_add" onClick={(e) => { addFunction(e); sendRequest(user.id); repIncrement(logged.id); }}>{add}</button>
                                 )
                             })}
                            </div>
                        }
                        </>
                    )})}
                </div>
            </div>
        </div>

        <div className="wrapper">
            <div className="margin-100">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Amis</button>
                </li>
            </ul>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">                
                        <div className="feed-profil bg-white1">
                            <div className="friendlist">
                            {friendlist.map((friend, index) => (
                            <Link to={`/users/${friend.id}`}>
                                <div className="friend" id={friend.id}>
                                    <img className="img_friend" src={Avatar} alt="avataruser"/>
                                    <p>{friend.firstName} {friend.lastName}</p>
                                    <p>{friend.email}</p>
                                </div>
                            </Link>
                            ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>            
        </>
    )
}

export default User;

