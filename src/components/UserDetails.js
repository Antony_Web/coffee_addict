import React, { useState, useEffect } from 'react'
import pictureUser1 from "../images/user1.jpg";
import { LoginContext } from '../contexts/loginStatus';
import { Link } from "react-router-dom";
import axios from "axios";

const UserDetails = () => {

    axios.defaults.withCredentials = true;

    const [loggedIn, setLoggedIn] = useState(false);
    const [loggedUser, setLoggedUser] = useState([]);
    const [id, setUserId] = useState('');

    useEffect(()=> {
        axios.get("http://localhost:3001/api/login")
        .then((res) => {
          if (res.data.loggedIn === true) {
            setUserId(res.data.user[0].id)
            setLoggedIn(true)
          } else {
            setLoggedIn(false)
          }
        })
    }, [])

    useEffect(()=> { //CONNECTION CHECK
        axios.get(`http://localhost:3001/api/users/${id}`)
        .then((res) => {
            setLoggedUser(res.data)
        })
      }, [id])
    
    return (
        <>
        <LoginContext.Provider value={{ loggedIn, setLoggedIn }}>
        <div className="bg-blues background-login">
        <div className="wrapper">
            <div className="profil_grid">
              <img className="img_profil" src={pictureUser1} alt="project number two."/>
              <div>
                {loggedUser.map((user, index) => {
                    return(
                        <>
                        <div className="profil_info">
                            <p>{ user.username }</p>
                            <p>{ user.firstName } { user.lastName }</p>
                            <p>{ user.email }</p>
                            <p>{ user.gender }</p>
                            <p>Status: 
                              { user.points < 6 ? " Débutant" : null}
                              { user.points > 5 && user.points < 26 ? " Apprentis" : null }
                              { user.points > 25 && user.points < 51 ? " Connaisseur" : null }
                              { user.points > 50 ? " Maître torréfacteur" : null}
                            </p>
                            <p>{user.points} points</p>
                        </div>

                        <div className="buttons">
                            <Link to={`/edit/${user.id}`} className="button">Modifier mon profil</Link>
                        </div>
                        </>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
        </LoginContext.Provider>
        </>
    )
}

export default UserDetails;


