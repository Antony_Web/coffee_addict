import React, { Component }  from 'react';
import { useState, useEffect } from 'react'
import axios from "axios";
import Meetup from '../../images/meetup-logo.png'
 
const MeetsList = () => {
    
    const [meets, setMeets] = useState([]);
    const [users, setUser] = useState([]);

    useEffect(()=> {
        axios.get("http://localhost:3001/api/login")
        .then((res) => {
          if (res) {
            setUser(res.data.user[0].id)
            getMeets(res.data.user[0].id)
          } else {

          }
        })
        }, [])

        // Fetch and show a single user's profile page by getting their ID.
        const getUserById = async (id) => {
            const response = await axios.get(`http://localhost:3001/api/users/${id}`)
            return response.data[0];
        }
        
        // Show pending requests
        const getMeets = async (userid) => {
            const requests = await axios.get(`http://localhost:3001/api/meets/${userid}`);

            const pendingResult = requests.data.result.map(async (request) => {
                const host_id = await getUserById(request.host_id);
                const invited_id = await getUserById(request.invited_id);
                return { host_id, invited_id, request };
            });

            console.log(await Promise.all(pendingResult));
            setMeets(await Promise.all(pendingResult))
            return await Promise.all(pendingResult);
        }
       
    return (
        <>
        { meets.map((meet, index) => (
            <div key={ meet.id } className="postfriend">
                <img src={Meetup} className="meetup" alt="img"/>
                { meet.host_id.id === users ? 
                    <p className="names">Vous avez un meetup avec {meet.invited_id.firstName} {meet.invited_id.lastName}</p> 
                    : 
                    <p className="names">Vous avez un meetup avec {meet.host_id.firstName} {meet.host_id.lastName}</p>
                }
                <p>au { meet.request.place }</p>
                <p></p>
                <p className="date meetdate">{ meet.request.date }</p>
            </div>
        ))}
        </>
    )
}
 
export default MeetsList;