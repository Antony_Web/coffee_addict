import React, { useState, useEffect } from "react"
import Axios from "axios"

const AddMeet = () => {

    // host_id, invited_id, place, date, 
    const [host_id, setHostId] = useState('');
    const [date, setDate] = useState('');
    const [place, setPlace] = useState('');
    const [invited_id, setInvitedId] = useState('');
    const [loggedUser, setLoggedUser] = useState(undefined);
    const [userConnected, setConnectedUser] = useState([]);

    useEffect(()=> { //CONNECTION CHECK
      Axios.get("http://localhost:3001/api/login").then((res) => {
          setHostId(res.data.user[0].id)
          getFriends(res.data.user[0].id)
      })
    }, [])

        // Fetch and show a single user's profile page by getting their ID.
    const getUserById = async (id) => {
      const response = await Axios.get(`http://localhost:3001/api/users/${id}`)
      return response.data[0];
    }

        //GET FRIENDLIST
        const [friendlist, setFriendlist] = useState([])

        const getFriends = async (userid) => {
            const friendlist = await Axios.get(`http://localhost:3001/api/friends/${userid}`);
            const pendingResult = friendlist.data.map(async (request) => {
                // receiver_id
                // sender_id
                if(request.receiver_id === userid) {
                    return await getUserById(request.sender_id);
                } else {
                    return await getUserById(request.receiver_id);
                }
            });
            
            console.log(await Promise.all(pendingResult));
            setFriendlist(await Promise.all(pendingResult))
            return await Promise.all(pendingResult);
        }

    const add = () => {
      Axios.post("http://localhost:3001/api/meets/add", 
      { host_id: host_id, date: date, place: place, invited_id: invited_id })
      .then((res)=> {
        if (res){
            console.log("Votre publication a été enregistrée !");
        } else {
            console.log("Votre publication n'a pas été enregistrée !");
        }
      })
    }

    // Reputation +25 for sender
    const repIncrement = (id) => {
      Axios.put(`http://localhost:3001/api/plus25reputation/${id}`, (req, res) => {
      })
      console.log(id)
  }

  console.log('host:', host_id)
  console.log('friendlist:', friendlist)


    return (
      <div className="background-register">
      <h2>Faire un meetup</h2>
      <form className="post-add" name="meet-form" id="meet-form" action="/Profil" >
        <input type="text" value={ host_id } hidden/>

        <div className="form-group content">
          <label htmlFor="content">Date</label>
          <input type="datetime-local" className="form-control" id="date" name="date"
            onChange={(e) => {
              setDate(e.target.value)
            }}
          />
        </div>

        <div className="form-group content">
          <label htmlFor="content">Place</label>
          <input type="text" className="form-control" id="place" name="place"
            onChange={(e) => {
              setPlace(e.target.value)
            }}
          />
        </div>

        <div className="form-group content">
        <label htmlFor="content">Ami(e)s à inviter</label>
          <select id="invited_id" name="invited_id" onChange={(e) => {
                setInvitedId(e.target.value)
              }}><br></br>
            <option hidden></option>
            {friendlist.map((friend, index) => (
            <option value={friend.id} name={friend.id} >{friend.firstName} {friend.lastName}</option>
            ))}
          </select>

        </div>
        <div className="buttons_grid">
            <button type="submit" className="button" onClick={(e) => { add(); repIncrement(host_id)}  }>Soumettre</button>
        </div>

      </form>
    </div>

    )
}

export default AddMeet