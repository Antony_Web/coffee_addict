import React, { Component }  from 'react';
import { useState, useEffect } from 'react'
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import Avatar from '../../images/background/avatar1.png'
 
const Feed = () => {
    
    const [user_id, setUserId] = useState([])
    const [posts, setPosts] = useState([]);
    const setPostsTrier = [];
    const [user, setUser] = useState([])
    //GET FRIENDLIST
    const [friendlist, setFriendlist] = useState([])

    useEffect(()=> {
        axios.get("http://localhost:3001/api/login")
        .then((res) => {
          if (res) {
            setUserId(res.data.user[0].id)
            setUser(res.data.user[0])
            console.log('le id du user:', res.data.user[0].id)
            //getPostsUser(res.data.user[0].id)
            getFriends(res.data.user[0].id)
            getFriendsPosts(res.data.user[0].id)
          } else {

          }
          
        })
        }, [])

        const getFriendsPosts = (user_id) => {
                axios.get(`http://localhost:3001/api/friends/posts/${user_id}`)
                .then((res)=> {
                    setPosts(res.data.result)
                    console.log('les posts ?', res.data.result)
                })
            console.log('Friendlist', friendlist)
        }

        console.log(setPostsTrier, 'les posts trie')

        // Fetch and show a single user's profile page by getting their ID.
        const getUserById = async (id) => {
            const response = await axios.get(`http://localhost:3001/api/users/${id}`)
            return response.data[0];
        }

        const getFriends = async (userid) => {
        const friendlist = await axios.get(`http://localhost:3001/api/friends/${userid}`);
        const pendingResult = friendlist.data.map(async (request) => {
            if(request.receiver_id === userid) {
                return await getUserById(request.sender_id);
            } else {
                return await getUserById(request.receiver_id);
            }
        });
        //console.log(await Promise.all(pendingResult));
        setFriendlist(await Promise.all(pendingResult))
        return await Promise.all(pendingResult);
    }


    return (
        <>
        
        <div className="feed">   
            {posts.map((post, index) => (
                <>
                    {friendlist.filter(friend => friend.id === post.user_id).map(filteredPost => (
                        <div className="postfriend">
                            <img src={Avatar} className="profil_feed" alt="img"/>
                            <Link to={`/users/${post.id}`}>
                            <p className="name">{post.firstName} {post.lastName}</p>
                            </Link>
                            <p className="date">{ new Date(post.created_at).toLocaleDateString()}, { new Date(post.created_at).toLocaleTimeString()}</p>
                            <p className="post_title">{post.title}</p>
                            <p>{post.content}</p>
                        </div>
                    ))}
                </>
                ))}
        </div>



        </>
    )
}
 
export default Feed;

