import React, { Component }  from 'react';
import { useState, useEffect } from 'react'
import axios from "axios";
import Avatar from '../../images/background/avatar1.png'
 
const PostList = () => {
    
    const [user_id, setUserId] = useState([])
    const [posts, setPosts] = useState([]);
    const [user, setUser] = useState([])

    useEffect(()=> {
        axios.get("http://localhost:3001/api/login")
        .then((res) => {
          if (res) {
            setUserId(res.data.user[0].id)
            setUser(res.data.user[0])
            console.log('le id du user:', res.data.user[0].id)
            getPostsUser(res.data.user[0].id)

          } else {

          }
        })
        }, [])

        // Fetch and show a single user's profile page by getting their ID.
        const getUserById = async (id) => {
            const response = await axios.get(`http://localhost:3001/api/users/${id}`)
            return response.data[0];
        }

        // Fetch and show a single user's profile page by getting their ID.
        const getPostsById = async (id) => {
            const response = await axios.get(`http://localhost:3001/api/posts/${id}`)
            return response.data[0];
        }
            
        useEffect(() => {
            getPosts();
        }, [user_id]);
    
        const getPosts = () => {
            axios.get(`http://localhost:3001/api/posts/${user_id}`)
            .then((res)=> {
                setPosts(res.data.result);
                console.log('les createdat::::::::', res.data.result)
                for(let i = 0; i < res.data.result.lenght; i++){
                    
                }
                console.log('tous les posts du user', res.data.result)

            })
            .catch(error => {
                console.log(error)
            })
        };

        // State for pending requests
        const [pending, setPending] = useState([])

        // Show pending requests
        const getPostsUser = async (userid) => {
            const requests = await axios.get(`http://localhost:3001/api/posts/${userid}`);

            console.log('la request:', requests);

            const pendingResult = requests.data.result.map(async (request) => {
                const user = await getUserById(request.user_id);
                //const post = await getPostsById(request.post);
                console.log('le resultat du getPostsUser:', user, 'les posts', posts)
                return { user, request };
            });

            console.log(await Promise.all(pendingResult));
            setPending(await Promise.all(pendingResult))
            return await Promise.all(pendingResult);
        }
 
    return (
<>
                { pending.map((post, index) => (
                <div className="postfriend">
                    <img src={Avatar} className="profil_feed" alt="img"/>
                    <p className="name">{post.user.firstName} {post.user.lastName}</p>
                    <p className="date">{ new Date(post.request.created_at).toLocaleDateString()}, { new Date(post.request.created_at).toLocaleTimeString()}</p>
                    <p className="post_title">{post.request.title}</p>
                    <p>{post.request.content}</p>
                </div>
                    )) }
</>
    )
}
 
export default PostList;