import React, { useState, useEffect } from "react"
import axios from 'axios';
import { useParams } from "react-router";

const AddPost = () => {

    const [user_id, setUserId] = useState("");
    const [user, setUser] = useState("");
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const id = useParams()
    

    useEffect(()=> {
      axios.get("http://localhost:3001/api/login")
      .then((res) => {
        setUserId(res.data.user[0].id)
        setUser(res.data.user)
      })
    }, [id])

    // Add Post
    const add = () => {
      axios.post("http://localhost:3001/api/posts/add", 
      { title: title, content: content, user_id: user_id })
      .then((res)=> {
        if (res){
            console.log("Votre publication a été enregistrée !");
        } else {
            console.log("Votre publication n'a pas été enregistrée !");
        }
      })
    };

    // Reputation +10 for posting
    const repIncrement = (id) => {
      axios.put(`http://localhost:3001/api/plus10reputation/${id}`, (req, res) => {
      })
      console.log(id)
  }

    console.log( 'le id:', user_id)


    return (
     <div className="background-register">
      <h2>Ajouter une publication</h2>
      <form className="post-add" name="register-form" action="/Profil" >

      <div className="form-group title">
        <label htmlFor="title">Titre</label>
        <input type="text" className="form-control" id="title" name="title"
          onChange={(e) => {
            setTitle(e.target.value)
          }}/>
      </div>

      <div className="form-group content">
        <label htmlFor="content">Contenu</label>
        <textarea type="text" className="form-control" id="content" name="content"
          onChange={(e) => {
            setContent(e.target.value)
          }}
        />
      </div>

      <div className="form-group">
        <input hidden type="text" className="form-control" id="user_id" name="user_id" defaultValue={user_id}
        />
      </div>

      
        <div className="buttons_grid">
            <button type="submit" className="button" onClick={()=> { add(); repIncrement(user_id); }}>Soumettre</button>
        </div>

      </form>
    </div>

    )
}

export default AddPost
