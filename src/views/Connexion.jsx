// == IMPORTS ==
import React, {useState, useEffect} from 'react'
import Axios from 'axios';
import { LoginContext } from '../contexts/loginStatus';
import { Redirect, useHistory } from 'react-router';

// COMPONENT
const Connexion = () => {

  Axios.defaults.withCredentials = true;
  //CONNEXION INFO REQUIREMENT
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  //LOGIN STATUS AND STATES
  const [loginStatus, setLoginStatus] = useState([]);
  const [loggedIn, setLoggedIn] = useState(false);
  const [loggedUser, setLoggedUser] = useState([]);

  const history = useHistory(); //use history navigation

  //Error status 
  const [usernameErr, setUsernameErr] = useState({});
  const [passwordErr, setPasswordErr] = useState({});

  const onSubmit = (e) => {
    const isValid =formValidation();
    if (isValid) {
      login()
    }else 
      e.preventDefault();
  }

  const login = () => { // LOGIN FUNCTION
    Axios.post("http://localhost:3001/api/login", {username: username, password: password})
    .then((res) => {
      if (res.data.message) {
        setLoginStatus(res.data.message)
        setLoggedIn(false)
        console.log("wrong username or password")
      } else {
        setLoginStatus(res.data[0].email)
        setLoggedIn(true)
        history.push('/')
        window.location.reload(false);
        console.log("logged in success!")
      }
    })
  }

  const formValidation = () => {
    
    const usernameErr = {};
    const passwordErr = {};
    let isValid = true;

    //validation du champ nom d'utilisateur
    if(username.trim().length === 0){
      usernameErr.usernameEmpty = "Le champ nom d'utilisateur ne peut pas être vide";
      isValid = false;
    }else if(username.trim().length < 5){
      usernameErr.usernameShort = "Le nom d'utilisateur est trop court";
      isValid = false;
    }else if(!username.match('^[a-zA-Z0-9]+')){
      usernameErr.usernamePattern = "Le nom d'utilisateur ne peut contenir que des lettres et des chiffres";
      isValid = false;
    }

    //Validation du mot de passse
    if(password.trim().length === 0){
      passwordErr.passwordEmpty = "Le champ mot de passe ne peut pas être vide";
      isValid = false;
    }else if(password.trim().length < 6){
      passwordErr.passwordShort = "Le mot de passe est trop court";
      isValid = false;
    }else if(!password.match('^(?=.*?[A-Za-z])(?=.*?[0-9]).{6,}$')){
      passwordErr.passwordPattern = "Le format du mot de passe n'est pas valide";
      isValid = false;
    }

    setUsernameErr(usernameErr);
    setPasswordErr(passwordErr);

    login();
  }


  useEffect(()=> { //CONNECTION CHECK
    Axios.get("http://localhost:3001/api/login").then((res) => {
      if (res.data.loggedIn === true) {
        setLoggedUser([res.data.user[0]])
        console.log(res.data.user[0])
        setLoggedIn(true)
      } else {
        setLoggedIn(false)
      }
    })
  }, [])

  // RENDERING
    return (
      <LoginContext.Provider value={{ loggedIn, setLoggedIn }}>

      <div className="background-login">
      <div onSubmit= {onSubmit} className="form">
      <h2 className="connexion fw-bolder">Connexion</h2>
      
      <h5 className="alert-info text-center">{loginStatus}</h5>

        <div className="form-group">
          <label className="login-label"  htmlFor="username">Nom d'utilisateur</label>
          <input
            type="text"
            className="form-control"
            id="username"
            name="username"
            aria-describedby="emailHelp"
            onChange={(e) => {
              setUsername(e.target.value)
            }}
          />
          {Object.keys(usernameErr).map((key)=>{
            return <div style={{color: 'red'}}>{usernameErr[key]}</div>
          })}
        </div>

        <div className="form-group">
          <label className="login-label" htmlFor="password">Mot de passe</label>
          <input
            type="password"
            className="form-control"
            id="password"
            name="password"
            onChange={(e) => {
              setPassword(e.target.value)
            }}
          />
          {Object.keys(passwordErr).map((key)=>{
            return <div style={{color: 'red'}}>{passwordErr[key]}</div>
          })}
        </div>

        <div className="buttons_grid">
          <p>Vous n'avez pas de compte ? <a href="/Register">Inscription</a> </p>
          <button type="submit" className="button" onClick={formValidation}>Se connecter</button>
        </div>

      </div>
      
      </div>
      </LoginContext.Provider>
    )
}

export default Connexion;

