// IMPORTS
import React, { useState, useEffect } from 'react'
import Caroussel from '../components/Caroussel';
import Feed from "../components/posts/Feed";
import Axios from 'axios';
// import des images
import FourthImage from '../images/FourthImage.jpg'

// import de bootstrap
import Button from 'react-bootstrap/Button'
import Card from 'react-bootstrap/Card'

const Home = () => {

    Axios.defaults.withCredentials = true;
    const [loggedIn, setLoggedIn] = useState(false);
    const [loggedUser, setLoggedUser] = useState([]);
 
    useEffect(()=> { //CONNECTION CHECK
        Axios.get("http://localhost:3001/api/login").then((res) => {
          if (res.data.loggedIn === true) {
            setLoggedUser([res.data.user[0]])
            console.log(res.data.user[0])
            setLoggedIn(true)
          } else {
            setLoggedIn(false)
          }
        })
      }, [])

    return (
        <div className="App">
            <div className="wrapper">
                <div className="carrousel-grid">
                    <Caroussel />
                </div>
            </div>

            <div className="background-register">
                <div className="bg-blue">
                    <div className="wrapper max">
                    <div className="carrousel-grid grid">
                        <img src={FourthImage} alt="img"/>
                        <div className="text-home">
                            <h3>Recommandations</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                    </div>
                    </div>
                </div>
                { loggedIn ?
                    <div className="top">
                        <Feed/>
                    </div>
                :

                <div className="top">
                    <div></div>
                </div>
                }

            </div>
        </div> 

    );
}

export default Home
