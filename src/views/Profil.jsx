import React, { useState, useEffect } from 'react'
import { LoginContext } from '../contexts/loginStatus';
import Axios from "axios";
import { Link } from "react-router-dom";
import UserDetails from '../components/UserDetails';
import PostList from '../components/posts/Post';
import MeetsList from '../components/meets/Meets';
import FriendList from '../components/friends/Friends'
 
// component
const Profil = () => {

    Axios.defaults.withCredentials = true;
    const [loggedIn, setLoggedIn] = useState(false);
    const [loggedUser, setLoggedUser] = useState([]);
 
    useEffect(()=> { //CONNECTION CHECK
        Axios.get("http://localhost:3001/api/login").then((res) => {
          if (res.data.loggedIn === true) {
            setLoggedUser([res.data.user[0]])
            console.log(res.data.user[0])
            setLoggedIn(true)
          } else {
            setLoggedIn(false)
          }
        })
      }, [])

    const deleteFriend = (id) => {
        Axios.delete(`http://localhost:3001/api/refuse/${id}`).then((res) => {
        })
        console.log(id)
    }

    function refreshPage() {
        window.location.reload(false);
    }

    return (
        <>
        <LoginContext.Provider value={{ value1: [loggedUser, setLoggedUser] }}>
        
        <UserDetails/>
        
        <div className="wrapper">
            <div className="margin-100">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Mes amis</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-meetup-tab" data-bs-toggle="pill" data-bs-target="#pills-meetup" type="button" role="tab" aria-controls="pills-meetup" aria-selected="false">Mes meetups</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Mes publications</button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">

            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">                
                <div className="feed-profil">
                    <FriendList/>
                </div>
            </div>

            <div class="tab-pane fade" id="pills-meetup" role="tabpanel" aria-labelledby="pills-meetup-tab">
                <div className="feed-profil">
                    <div className="link">
                    <Link to={`/addmeets`} className="button add_post">Organiser un meetup</Link>
                    </div>
                        <MeetsList/>
                </div>
            </div>

            <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <div className="feed-profil">
                        <div className="link">
                            <Link to={`/posts/add`} className="button add_post">Ajouter une publication</Link>
                        </div>
                            <PostList/>
                    </div>
                </div>
            </div>
            </div>
        </div>
        
        </LoginContext.Provider>
        </>
    )
}

export default Profil;



