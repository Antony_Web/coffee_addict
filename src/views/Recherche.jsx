import { Link } from "react-router-dom";
import React, { useState, useEffect } from 'react'
import axios from 'axios';

const Search = () => {

    const [data, setData] = useState([]);
    const [name, setFullName] = useState('')
    const [foundUsers, setFoundUsers] = useState(data);

    useEffect(() => {
        getUsers();
    }, []);

    const getUsers = () => {
        axios.get(`http://localhost:3001/api/users`)
            .then((res)=> {
                setData(res.data);
        })
    };
  
    const filter = (e) => {
      const keyword = e.target.value;
  
      if (keyword !== '') {
        const results = data.filter((user) => {
            const fullName = user.firstName.toLowerCase() + user.lastName.toLowerCase()
            fullName.trim().toLowerCase()
            
            return fullName.match(keyword.toLowerCase());
        });
        setFoundUsers(results);
      } else {
        setFoundUsers('');
        // If the text field is empty, show nothing
      }
  
      setFullName(keyword);
    };

    return (
        <div className="nav-link">
            <div className="input-group">
            <input
                type="search"
                value={name}
                onChange={filter}
                className="search_bar form-control"
                placeholder="Recherche..."
            />
            
            <ul>
                <li>
                {foundUsers && foundUsers.length > 0 ? (
                foundUsers.map((user) => (
                    <div key={user.id} className="choice">
                    <Link to={`/users/${user.id}`} className="search_link">
                    <div className="searching">
                        <p>{user.firstName}</p>
                        <p>{user.lastName}</p>
                    </div>
                    </Link>
                    </div>
                ))
                ) : (
                <p className="result">0 résultat</p>
                )}
                </li>
            </ul>
            </div>
        </div>
    )
}

export default Search
