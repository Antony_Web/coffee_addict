import React, {useState} from 'react'
import Axios from 'axios';
import { useHistory } from 'react-router';

// import express from 'express';
// const app = express();



const Register = () => {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [gender, setGender] = useState("");
  const [registered, setRegistered] = useState(false);
  

  const history = useHistory(); //use history navigation

  const [firstNameErr, setFirstNameErr] = useState({});
  const [lastNameErr, setLastNameErr] = useState({});
  const [usernameErr, setUsernameErr] = useState({});
  const [emailErr, setEmailErr] = useState({});
  const [passwordErr, setPasswordErr] = useState({});


  const onSubmit = (e) => {
    const isValid =formValidation();
    if (isValid) {
      register()
    }else 
      e.preventDefault();
  }

  const formValidation =() => {
    const firstNameErr = {};
    const lastNameErr = {};
    const usernameErr = {};
    const emailErr = {};
    const passwordErr = {};
    let isValid = true;
    
    //validation du champ prénom
    if(firstName.trim().length === 0){
      firstNameErr.firstNameEmpty = "Le champ prénom ne peut pas être vide";
      isValid = false;
    }else if(firstName.trim().length < 5){
      firstNameErr.firstNameShort = "Le prénom est trop court";
      isValid = false;
    }else if(!firstName.match('^[a-zA-Z0-9]+')){
      firstNameErr.firstNamePattern = "Le prénom ne peut contenir que des lettres et des chiffres";
      isValid = false;
    }

    //validation du champ nom
    if(lastName.trim().length === 0){
      lastNameErr.lastNameEmpty = "Le champ nom ne peut pas être vide";
      isValid = false;
    }else if(lastName.trim().length < 5){
      lastNameErr.lastNameShort = "Le nom est trop court";
      isValid = false;
    }else if(!lastName.match('^[a-zA-Z0-9]+')){
      lastNameErr.lastNamePattern = "Le nom ne peut contenir que des lettres et des chiffres";
      isValid = false;
    }

    //validation du champ nom d'utilisateur
    if(username.trim().length === 0){
      usernameErr.usernameEmpty = "Le champ nom d'utilisateur ne peut pas être vide";
      isValid = false;
    }else if(username.trim().length < 5){
      usernameErr.usernameShort = "Le nom d'utilisateur est trop court";
      isValid = false;
    }else if(!username.match('^[a-zA-Z0-9]+')){
      usernameErr.usernamePattern = "Le nom d'utilisateur ne peut contenir que des lettres et des chiffres";
      isValid = false;
    }

    // Validation de l'adresse courriel
    if(email.trim().length === 0){
      emailErr.emailEmpty = "Le champ courriel ne peut pas être vide";
      isValid = false;
    }else if(!email.match('^[a-zA-Z0-9._:$!%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]$')){
      emailErr.emailPattern = "Le format du courriel n'est pas valide";
      isValid = false;
    }

    //Validation du mot de passse
    if(password.trim().length === 0){
      passwordErr.passwordEmpty = "Le champ mot de passe ne peut pas être vide";
      isValid = false;
    }else if(password.trim().length < 6){
      passwordErr.passwordShort = "Le mot de passe est trop court";
      isValid = false;
    }else if(!password.match('^(?=.*?[A-Za-z])(?=.*?[0-9]).{6,}$')){
      passwordErr.passwordPattern = "Le format du mot de passe n'est pas valide";
      isValid = false;
    }

    setFirstNameErr(firstNameErr);
    setLastNameErr(lastNameErr);
    setUsernameErr(usernameErr);
    setEmailErr(emailErr);
    setPasswordErr(passwordErr);
    return isValid;

  }

  

  const register = () => {
    Axios.post("http://localhost:3001/api/register", {firstName: firstName, lastName: lastName, username: username, password: password, email: email, gender: gender}).then((res)=> {
      if (res){
        console.log("You are now registered!");
        history.push('/Connexion')
        window.location.reload(false);
      }
    })
    setRegistered(true);
  };


    return (
     <div className="background-register mt-5">

    <h2>Inscription</h2>
      
        
      <form onSubmit= {onSubmit} className="form-register wrapper" id="registerForm" action="/Connexion">

      <div className="form-group">
        <label className="inscription-label" htmlFor="firstName">Prénom</label>
        <input type="text" className="form-control" id="firstName" name="firstName"  
        pattern="^[a-zA-Z0-9]+" required
          onChange={(e) => {
            setFirstName(e.target.value)
          }}/>
          {Object.keys(firstNameErr).map((key)=>{
            return <div style={{color: 'red'}}>{firstNameErr[key]}</div>
          })}
      </div>

      <div className="form-group">
        <label className="inscription-label" htmlFor="lastName">Nom</label>
        <input type="text" className="form-control" id="lastName" name="lastName" 
        pattern="^[a-zA-Z0-9]+" required
          onChange={(e) => {
            setLastName(e.target.value)
          }}
        />
        {Object.keys(lastNameErr).map((key)=>{
            return <div style={{color: 'red'}}>{lastNameErr[key]}</div>
          })}
        
      </div>

      <div className="radio-group">
          <label className="btn " htmlFor="gender">Homme / Il</label>
          <input type="radio" name="gender" id="gender1" autoComplete="off" value="male" onChange={(e) => {
                setGender(e.target.value)
              }}/>
          <label className="btn " htmlFor="gender">Femme / Elle</label>
          <input type="radio" name="gender" id="gender2" autoComplete="off" value="female" onChange={(e) => {
                setGender(e.target.value)
              }}/>
          <label className="btn " htmlFor="gender">Autres / Iel</label>
          <input type="radio" name="gender" id="gender3" autoComplete="off" value="other" onChange={(e) => {
                setGender(e.target.value)
              }}/>
      </div>
      
      <div className="form-group input">
        <label className="inscription-label" htmlFor="username">Nom d'utilisateur</label>
        <input
          type="text" 
          className="form-control"
          id="username"
          name="username" 
          pattern="^[a-zA-Z0-9]+" required
          onChange={(e) => {
            setUsername(e.target.value)
          }}
        />
        {Object.keys(usernameErr).map((key)=>{
            return <div style={{color: 'red'}}>{usernameErr[key]}</div>
          })}
        
      </div>
      
      <div className="form-group input">
        <label className="inscription-label" htmlFor="email">Adresse courriel</label>
        <input
          type="email" 
          className="form-control"
          id="email"
          aria-describedby="emailHelp"
          pattern='^[a-zA-Z0-9._:$!%-]+@[a-zA-Z0-9.-]+.[a-zA-Z]$' required
          onChange={(e) => {
            setEmail(e.target.value)
          }}
        />

        {Object.keys(emailErr).map((key)=>{
            return <div style={{color: 'red'}}>{emailErr[key]}</div>
          })}
      
      </div>
      <div className="form-group input">
        <label className="inscription-label" htmlFor="password">Mot de passe</label>
        <input
          type="password" 
          className="form-control"
          id="password"
          name="password"  
          pattern= '^(?=.*?[A-Za-z])(?=.*?[0-9]).{6,}$' required
          onChange={(e) => {
            setPassword(e.target.value)
          }}
        />
        {Object.keys(passwordErr).map((key)=>{
            return <div style={{color: 'red'}}>{passwordErr[key]}</div>
          })}
        
      </div>
        <div className="buttons_grid">
            <p><span>Déjà un compte ? </span><a href="/Connexion">Connectez-vous</a> </p>
            <button type="submit" className="button" onClick={formValidation}>Soumettre</button>
        </div>
      </form>
        
    </div>
    )
  }


export default Register
